#!/usr/bin/env bats

if [ -z $chruby_version ]; then
  echo "Error: can't run tests"
  echo "Please provide the chruby_version environment variable"
  exit 1
fi

@test "chruby-exec binary is found in $PATH" {
  run which chruby-exec

  [ "$status" -eq 0 ]
}

@test "chruby-exec is installed in version ${chruby_version}" {
  run /usr/local/bin/chruby-exec --version

  current_version=$(echo "${output}" | sed "s/^chruby\sversion\s\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/")

  echo "DEBUG: current version: ${current_version}"
  echo "DEBUG: expected version: ${chruby_version}"

  [ "${current_version}" == "${chruby_version}" ]
}
