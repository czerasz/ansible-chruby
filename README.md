# Ansible Chruby Role

A role used to install [`chruby`](https://github.com/postmodern/chruby).

## Installation

Install the role with:

```
git clone https://gitlab.com/czerasz/ansible-chruby.git /etc/ansible/roles/czerasz.chruby
```

## Usage

```
---

- name: Install chruby
  hosts: all
  roles:
    - role: czerasz.chruby
      vars:
        version: <version>
        download_checksum: "sha256:<checksum>"
        signature_checksum: "sha256:<checksum>"
```

Or for default `0.3.9` version:

```
---

- name: Install chruby
  hosts: all
  roles:
    - role: czerasz.chruby
      vars:
        version: 0.3.9
```

## Role Variables

| name | description | default |
| --- | --- | --- |
| `version` | `chruby` version | `0.3.9` |
| `download_checksum` | The `sha256` checksum of the downloaded archive file. Get the checksum with: `curl -Ls https://github.com/postmodern/chruby/archive/v<version>.tar.gz | sha256sum | sed 's/\(.*\)\s\s\-/\1/'` | `sha256:7220a96e355b8a613929881c091ca85ec809153988d7d691299e0a16806b42fd` |
| `signature_checksum` | The `sha256` checksum of the downloaded signature file. Get the checksum with: `curl -Ls https://raw.github.com/postmodern/chruby/master/pkg/chruby-<version>.tar.gz.asc | sha256sum | sed 's/\(.*\)\s\s\-/\1/'` | `sha256:5b5b4680b2dd1543443ae3fa00390a8dcd1f06e15ba94679f0b09816df51d4c3` |
| `sign_key_file_checksum` | The `sha256` checksum of the downloaded signature file | `sha256:cd66de33ae91d6d537cba2e73e2d45a29c3d7efdb117fd93aaf3b907fcac86c7` |
| `sign_gpg_key` | GPG key ID used to sign the checksum file | `B9515E77` |
| `sign_gpg_key_fingerprint` | GPG fingerprint of the key used to sign the checksum file | `04B2F3EA654140BCC7DA1B5754C3D9E9B9515E77` |

# Test

Requirements:

- [Docker Compose](https://docs.docker.com/compose/)

Run tests with:

```
docker-compose -f test/docker-compose.yml up
```
